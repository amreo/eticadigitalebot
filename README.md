# Bot gestione del gruppo di Etica Digitale

Questo è il codice sorgente del bot per la gestione del [gruppo telegram](https://t.me/eticadigitale) di [etica digitale](https://eticadigitale.org).

## Funzioni del bot

* Antiflood: limita il numero di messaggi che un utente può mandare entro un certo tempo. L'utente che supera questo limite viene bloccato e gli viene chiesto di risolvere un captcha, il quale consiste nel premere l'emoji corretta.
* Comando /punisci: al primo utilizzo su un utente quest'ultimo viene mutato per 7 giorni, al secondo utilizzo viene bandito dal gruppo.
* Comando /perdona: toglie le limitazioni all'utente, ne azzera gli avverimenti (del comando /punisci), gli permette di rientrare nel gruppo se era stato bandito.
* Comando /banhammer: bandisce istantaneamente l'utente dal gruppo.
* Rimpiazza link: cancella il messaggio dell'utente e ne invia uno nuovo con il link rimpiazzato (se contiene un link di youtube, reddit e/o instagram).

## Come avviare il bot

### Tramite Docker
* Clonare la repository e spostarsi nella cartella
* Rinomina il file [config.json.example](./config.json.example) in `config.json` e configura il bot a tuo piacemento
* `docker-compose up -d`

### Senza docker
* Clona questa repository
* Rinomina il file [config.json.example](./config.json.example) in `config.json` e configura il bot a tuo piacemento
* `npm install`
* `npm run build`
* `npm run start`

**Nota**: il programma fa utilizzo anche del file `db.sqlite` dove sono memorizzati alcuni dati persistenti. Per cui è necessario importarlo se ad esempio, ci si sta spostando da un server ad un altro.

## Licenza

Tutti i file all'interno di questa repository sono protetti da licenza [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html), una copia della licenza è disponibile nel file [LICENSE](./LICENSE)
