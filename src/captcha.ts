import { Composer, InlineKeyboard } from 'grammy';
import { CustomContext } from './bot';
import { config } from './config';
import { getUserTag } from './utils';

export const captcha = new Composer<CustomContext>();

captcha.on('callback_query:data', async (ctx) => {
  const sender = ctx.callbackQuery.from;
  const limitedUser = ctx.session.limitedUsers[sender.id];
  if (limitedUser) {
    if (ctx.callbackQuery.data.includes(`captcha_${sender.id}`)) {
      if (ctx.callbackQuery.data.includes(`_${limitedUser.emoji}`)) {
        ctx.restrictChatMember(sender.id, {
          can_send_polls: true,
          can_change_info: true,
          can_invite_users: true,
          can_pin_messages: true,
          can_send_messages: true,
          can_send_media_messages: true,
          can_send_other_messages: true,
          can_add_web_page_previews: true,
        });
        // Delete user entry so he can't flood after successfully solving the captcha
        // Need this because of lines 47-53 of antiflood.ts file
        delete ctx.session.limitedUsers[sender.id];
        ctx.editMessageText(
          `${getUserTag(sender)} ha risolto il captcha con successo`,
          {
            parse_mode: 'MarkdownV2',
          },
        );
      } else {
        limitedUser.errors += 1;
        if (limitedUser.errors >= 2) {
          ctx.editMessageText(
            `${getUserTag(
              sender,
            )} ha sbagliato due volte il captcha ed è stato bloccato per sempre`,
            {
              parse_mode: 'MarkdownV2',
            },
          );
        } else {
          ctx.editMessageText(
            `${getUserTag(sender)} clicca *${
              config.captchaEmojis[limitedUser.emoji].description
            }* per risolvere il captcha\n*Errori*: ${limitedUser.errors}`,
            {
              reply_markup: generateCaptcha(sender.id),
              parse_mode: 'MarkdownV2',
            },
          );
        }
      }
    }
  } else {
    ctx.answerCallbackQuery();
  }
});

export const generateCaptcha = (userId: number): InlineKeyboard => {
  const shuffledEmojis: any[] = [];
  Object.keys(config.captchaEmojis).map((key) => {
    shuffledEmojis.push(config.captchaEmojis[key]);
  });
  for (let i = shuffledEmojis.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = shuffledEmojis[i];
    shuffledEmojis[i] = shuffledEmojis[j];
    shuffledEmojis[j] = temp;
  }

  const inlineKeyboard = new InlineKeyboard();

  shuffledEmojis.forEach((emoji) => {
    inlineKeyboard.text(emoji.emoji, `captcha_${userId}_${emoji.code}`).row();
  });

  return inlineKeyboard;
};
