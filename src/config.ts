export interface Config {
  environment: string;
  token: string;
  databaseFile: string;
  antiflood: {
    maxMessages: number;
    secondsRange: number;
  };
  captchaEmojis: {
    [code: string]: {
      code: string;
      emoji: string;
      description: string;
    };
  };
  blipBlopExplanationLink: string;
}
import * as _config from '../config.json';

export const config: Config = _config;
