import { addDays, getUnixTime } from 'date-fns';
import { CustomContext } from 'src/bot';
import { User } from '../user';
import { getRepository } from 'typeorm';
import { handleAdminCommand } from './admin-command-handler';
import { getUserTag } from '../utils';

export const punisciCommand = async (ctx: CustomContext) => {
  handleAdminCommand(ctx, async (userToPunish, reason) => {
    const userRespository = getRepository(User);
    const userFound = await userRespository.findOne({ id: userToPunish.id });
    if (!userFound) {
      ctx.reply(
        `L'utente ${getUserTag(userToPunish)} non è presente nel database`,
      );
      return;
    }
    if (await shouldBan(userFound)) {
      await ctx.banChatMember(userToPunish.id);
      userFound.warnings = 0;
      userFound.lastWarn = null;
      if (!reason) {
        ctx.reply(`L'utente ${getUserTag(userToPunish)} è stato rimosso\\.`, {
          parse_mode: 'MarkdownV2',
        });
      } else {
        ctx.reply(
          `L'utente ${getUserTag(
            userToPunish,
          )} è stato rimosso\\.\nMotivo: ${reason}`,
          { parse_mode: 'MarkdownV2' },
        );
      }
    } else {
      await ctx.restrictChatMember(
        userToPunish.id,
        {
          can_send_polls: false,
          can_change_info: false,
          can_invite_users: false,
          can_pin_messages: false,
          can_send_messages: false,
          can_send_media_messages: false,
          can_send_other_messages: false,
          can_add_web_page_previews: false,
        },
        { until_date: getUnixTime(addDays(Date.now(), 7)) },
      );
      userFound.warnings = 1;
      userFound.lastWarn = getUnixTime(Date.now());
      if (!reason) {
        ctx.reply(
          `L'utente ${getUserTag(
            userToPunish,
          )} è stato silenziato per 7 giorni\\.`,
          {
            parse_mode: 'MarkdownV2',
          },
        );
      } else {
        ctx.reply(
          `L'utente ${getUserTag(
            userToPunish,
          )} è stato silenziato per 7 giorni\\.\nMotivo: ${reason}`,
          { parse_mode: 'MarkdownV2' },
        );
      }
    }

    userRespository.save(userFound);
  });
};

async function shouldBan(user: User): Promise<boolean> {
  const olderThanOneYear = getUnixTime(Date.now()) - user.lastWarn >= 31536000;
  if (user.warnings === 0 || olderThanOneYear) {
    return false;
  }
  return true;
}
