import { Context, NextFunction } from 'grammy';
import { User } from '@grammyjs/types';

export interface UtilsContext extends Context {
  isAdmin(userId: number): Promise<boolean>;
}

export const utilsMiddleware = (ctx: UtilsContext, next: NextFunction) => {
  ctx.isAdmin = async (userId: number) => {
    const admins = await ctx.getChatAdministrators();
    const isAdmin = admins.some((v) => v.user.id === userId);
    return isAdmin;
  };
  return next();
};

export function getUserTag(user: User) {
  if (user.username) {
    return `@${escapeText(user.username)}`;
  }

  return `[${escapeText(user.first_name)}](tg://user?id=${user.id})`;
}

export function escapeText(text: string) {
  return text
    .replace(/_/gm, '\\_')
    .replace(/\*/gm, '\\*')
    .replace(/\[/gm, '\\[')
    .replace(/\]/gm, '\\]')
    .replace(/\(/gm, '\\(')
    .replace(/\)/gm, '\\)')
    .replace(/~/gm, '\\~')
    .replace(/`/gm, '\\`')
    .replace(/>/gm, '\\>')
    .replace(/#/gm, '\\#')
    .replace(/\+/gm, '\\+')
    .replace(/-/gm, '\\-')
    .replace(/=/gm, '\\=')
    .replace(/\|/gm, '\\|')
    .replace(/\{/gm, '\\{')
    .replace(/\}/gm, '\\}')
    .replace(/\./gm, '\\.')
    .replace(/!/gm, '\\!');
}
