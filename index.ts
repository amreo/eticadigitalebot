import { bot } from './src/bot';
import { config } from './src/config';
import { Connection, createConnection } from 'typeorm';

let connection: Connection;

const start = async () => {
    console.log('Starting the bot...');
    console.log('Press Ctrl+C to stop the bot');

    connection = await createConnection({
        type: 'sqlite',
        database: config.databaseFile,
        synchronize: config.environment === 'development',
        entities: ['dist/src/user.js'],
    });
    await bot.start({
        drop_pending_updates: true
    });
};

const stop = async () => {
    console.log('Stopping the bot...');
    await bot.stop();
    await connection.close();
};

start();

process.once('SIGINT', () => {
    stop();
});
process.once('SIGTERM', () => {
    stop();
});
